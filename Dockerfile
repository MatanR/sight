FROM docker.io/library/node:18-alpine as client
WORKDIR /client

COPY client/ /client/

RUN npm install && npm run build

# Production image, copy all the files and run next
FROM docker.io/library/node:18-alpine
WORKDIR /server

COPY server/ /server/
COPY --from=client /client/build /server/public

RUN npm install && npm run build

EXPOSE 3000
CMD ["npm", "run", "start:prod"]