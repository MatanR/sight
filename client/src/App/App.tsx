import "./App.css";
import { Row, Column } from "../Layout";
import Sidebar from "../Components/Sidebar";
import Routes from "../Routes";
import ThemeProvider from "../Providers/ThemeProvider";
import { useEffect } from "react";
import { useSystems } from "../Providers/SystemsProvider";

function App() {
  const { fetchSystems } = useSystems();

  useEffect(() => {
    fetchSystems();
  }, []);

  return (
    <div className="App">
      <ThemeProvider>
        <Row>
          <Sidebar />
          <Column>
            <Routes />
          </Column>
        </Row>
      </ThemeProvider>
    </div>
  );
}

export default App;
