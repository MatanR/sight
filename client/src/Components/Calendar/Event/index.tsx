import React from 'react'
import { Appointments } from '@devexpress/dx-react-scheduler-material-ui';

const bgColorsMap: any = {
  'UPGRADE': '#f29b0f',
  'SHUTDOWN': '#f66464',
  'OTHER': '#64b5f6'
}

const Event = ({ children, style, onClick, data = {}, ...restProps }: any) => {
  return (
    <Appointments.Appointment
      {...restProps}
      style={{
        ...style,
        backgroundColor: bgColorsMap[data.action],
        borderRadius: '8px',
      }}
      onClick={() => onClick(data)}
    >
      {children}
    </Appointments.Appointment>
  )
}

export default Event
