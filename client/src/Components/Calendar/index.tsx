import React, { useEffect, useState } from 'react';
import { Paper } from '@mui/material';
import { ViewState } from '@devexpress/dx-react-scheduler';
import {
  Scheduler,
  MonthView,
  Toolbar,
  TodayButton,
  Appointments,
  DateNavigator,
} from '@devexpress/dx-react-scheduler-material-ui';
import Event from './Event';
import './Calendar.css'
import { useEvents } from '../../Providers/EventsProvider';
import EventModal from '../EventModal'

export default () => {
  const { events, fetchEvents } = useEvents();
  const [date, setDate] = useState(new Date())
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [selectedEventData, setSelectedEventData] = useState({});

  const changeModalState = () => setIsOpen(prev => !prev);

  useEffect(() => {
    const abort = fetchEvents()

    return () => abort()
  }, [date])

  const handleEventClick = (data: any) => {
    setSelectedEventData({ ...data, systemId: data?.affects?.[0]?.uid })
    setIsOpen(true)
  }

  return <Paper sx={{ height: '100%', width: '100%' }}>
    <Scheduler data={events} locale='he-IL'>
      <ViewState currentDate={date} onCurrentDateChange={newDate => setDate(newDate)} />
      <MonthView />
      <Toolbar />
      <DateNavigator />
      <TodayButton messages={{ today: 'היום' }} />
      <Appointments appointmentComponent={props => <Event onClick={handleEventClick} {...props} />} />
    </Scheduler>
    <EventModal isOpen={isOpen} changeModalState={changeModalState} initialValues={selectedEventData} />
  </Paper>
}
