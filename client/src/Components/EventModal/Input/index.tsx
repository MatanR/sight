import { Typography, Box } from '@mui/material'
import { Row } from '../../../Layout';

interface Props {
    label: string;
    Controller: JSX.Element;
    labelWidth?: number;
    controllerWidth?: string;
}

const Input = ({ label, Controller, labelWidth = 120, controllerWidth = '100%' }: Props) =>
    <Row sx={{ width: 'auto', alignItems: 'center', margin: '5px 0' }}>
        <Typography sx={{ width: labelWidth, fontWeight: 'bold' }}>{label}</Typography>
        <Box sx={{ width: controllerWidth }}>{Controller}</Box>
    </Row>

export default Input;