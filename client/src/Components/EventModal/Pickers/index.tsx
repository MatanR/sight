import { useTheme } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { Controller } from 'react-hook-form';
import { TextField } from '@mui/material';
import { Column, Row } from '../../../Layout';
import Input from '../Input';
import { MobileDateTimePicker } from '@mui/x-date-pickers';

const Pickers = ({ control }: any) => {
    const theme = useTheme();

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Column>
                <Row>
                    <Input label='מתאריך' labelWidth={100} Controller={
                        <Controller
                            name={"startDate"}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                                <MobileDateTimePicker
                                    ampm={false}
                                    closeOnSelect={true}
                                    views={['year', 'day', 'hours']}
                                    InputProps={{ disableUnderline: true }}
                                    inputFormat="DD/MM/YYYY HH:MM"
                                    value={value}
                                    onChange={onChange}
                                    renderInput={(params) => <TextField sx={{ input: { color: theme.palette.secondary.main, fontWeight: 'bold' } }} variant='standard' {...params} />}
                                />
                            )}
                        />}
                    />
                    <Input label='עד תאריך' labelWidth={120} Controller={
                        <Controller
                            name={"endDate"}
                            control={control}
                            render={({ field: { onChange, value } }) => (
                                <MobileDateTimePicker
                                    ampm={false}
                                    closeOnSelect={true}
                                    views={['year', 'day', 'hours']}
                                    InputProps={{ disableUnderline: true }}
                                    inputFormat="DD/MM/YYYY HH:MM"
                                    value={value}
                                    onChange={onChange}
                                    renderInput={(params: any) => <TextField sx={{ input: { color: theme.palette.secondary.main, fontWeight: 'bold' } }} variant='standard' {...params} />}
                                />
                            )}
                        />}
                    />
                </Row>
            </Column>
        </LocalizationProvider>
    );
}

export default Pickers;