import { useState, useEffect } from 'react';
import { Modal, Box, TextField, Button, MenuItem, Typography } from '@mui/material';
import { Controller, useForm } from 'react-hook-form';
import { Column, Row } from '../../Layout';
import Input from './Input';
import Pickers from './Pickers';
import { form, modal, systemStyle } from './styles'
import { useSystems } from '../../Providers/SystemsProvider';
import { yupResolver } from '@hookform/resolvers/yup';
import { validationSchema } from './validationSchema'
import axios from 'axios';
import { useEvents } from '../../Providers/EventsProvider';
import { isNullishCoalesce } from 'typescript';

const defaultValues = () => {
    const nowDate = new Date()
    nowDate.setMinutes(0, 0, 0)

    return {
        name: '',
        description: '',
        manager: '',
        systemId: '',
        action: '',
        startDate: nowDate,
        endDate: nowDate,
    };
}

const typeOptions = [
    { value: "UPGRADE", label: "שדרוג" },
    { value: "SHUTDOWN", label: "השבתה" },
    { value: "OTHER", label: "אחר" }
];

// const networkOptions = [{ value: "apple", label: "תפוח" }]

const EventModal = ({ isOpen, changeModalState, initialValues }: any) => {
    const [isError, setIsError] = useState<String>("");
    const { handleSubmit, reset, control, watch, setValue } = useForm({
        defaultValues: defaultValues(),
        resolver: yupResolver(validationSchema)
    });

    useEffect(() => {
        console.log(initialValues)
        reset(initialValues)
    }, [initialValues])

    const { systems } = useSystems();
    const { fetchEvents } = useEvents();

    const onSubmit = async (data: any) => {
        try {
            await axios.post(`/api/events`, data)
            reset({ ...defaultValues() })
            changeModalState()
            fetchEvents()
        } catch (e: any) {
            if (e.response.status === 409) {
                const { name: eventName, systemName, manager } = e.response.data;

                setIsError(`במערכת ${systemName} מתקיים אירוע ${eventName} אשר מתנגש עם האירוע שיצרת, בחר תאריך אחר או צור קשר עם ${manager}`);
            }
            else
                setIsError(`קיימת שגיאה בנתונים`)
        }
    }

    const inputs = [
        {
            label: 'סוג אירוע', name: 'action', render: ({ field }: any) => (
                <TextField id='action' select SelectProps={{ IconComponent: () => null }} {...field} sx={{ width: "40%", '& .MuiSelect-select': { padding: '8px !important' } }}>
                    {typeOptions.map(({ value, label }, index) => <MenuItem value={value} key={index}>{label}</MenuItem>)}
                </TextField>
            )
        },
        {
            label: 'מוביל אירוע', name: 'manager', render: ({ field: { onChange, value } }: any) => (
                <TextField id='manager' sx={{ width: '95%' }} variant='standard' inputProps={{ autoComplete: "none" }} onChange={onChange} value={value} />
            )
        },
        {
            label: 'תיאור', name: 'description', render: ({ field: { onChange, value } }: any) => (
                <TextField id='description' sx={{ width: '95%' }} variant='standard' inputProps={{ autoComplete: "none" }} onChange={onChange} value={value} />
            )
        }
    ];

    const startDate = new Date(watch('startDate')).getTime()

    useEffect(() => {
        if (!initialValues) {
            setValue('endDate', new Date(startDate))
        }
    }, [startDate])

    return (
        <>
            <Modal className='modal' open={isOpen} onClose={changeModalState} sx={modal}>
                <Box sx={{ width: 400 }}>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <Column sx={form}>
                            <Input label='על מה אני עובד?' labelWidth={180} Controller={
                                <Controller
                                    name={"systemId"}
                                    control={control}
                                    render={({ field }) => (
                                        <TextField id='systemId' sx={systemStyle}
                                            label='סוג מערכת' select SelectProps={{ IconComponent: () => null }} {...field}>
                                            {systems.map(({ name, uid }: any) => <MenuItem value={uid} key={uid}>{name}</MenuItem>)}
                                        </TextField>
                                    )}
                                />}
                            />
                            <Controller
                                name={"name"}
                                control={control}
                                render={({ field: { onChange, value } }) => (
                                    <TextField id="name" sx={{ width: '95%', '& label': { left: 'auto' } }} variant='standard' inputProps={{ autoComplete: "none" }} onChange={onChange} value={value} label={"כותרת"} />
                                )}
                            />
                            <Pickers control={control} />
                            {
                                inputs.map(({ label, name, render }: any, index) =>
                                    <Input key={index} label={label} Controller={
                                        <Controller
                                            name={name}
                                            control={control}
                                            render={render}
                                        />}
                                    />)
                            }
                            <Row sx={{ justifyContent: !!isError ? 'space-between' : 'flex-end', marginTop: '5px' }}>
                                {!!isError && <Box sx={{ display: 'flex' }}>
                                    <Typography sx={{ color: 'red' }} variant='body2'>{isError}</Typography>
                                </Box>}
                                <Box sx={{ display: 'flex' }}>
                                    <Button id='add-btn' sx={{ borderRadius: 5, padding: '1px 8px' }} variant='contained' color='secondary' type='submit'>צור אירוע</Button>
                                </Box>
                            </Row>
                        </Column>
                    </form>
                </Box>
            </Modal>
        </>
    );
};

export default EventModal;