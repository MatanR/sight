export const modal = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
}

export const form = {
    direction: 'rtl',
    bgcolor: 'white',
    p: 4,
    borderRadius: 2,
};

const rtlLabel = {
    left: 'auto',
    right: 30,
    transformOrigin: 'top right',
    textAlign: 'right',
};

export const systemStyle = {
    '& label': { ...rtlLabel },
    '& legend': { textAlign: 'right' },
    width: "40%",
};
