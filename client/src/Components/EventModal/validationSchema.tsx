import * as yup from 'yup';

export const validationSchema = yup.object().shape({
    name: yup.string().required().matches(RegExp('^[a-zA-Z\u0590-\u05FF\u200f\u200e ]+$')),
    description: yup.string().required(),
    manager: yup.string().required().matches(RegExp('^[a-zA-Z\u0590-\u05FF\u200f\u200e ]+$')),
    // networkId: yup.string().required().matches(RegExp('^[a-zA-Z\u0590-\u05FF\u200f\u200e ]+$')),
    systemId: yup.string().required(),
    // status: yup.string().required().matches(RegExp('^[a-zA-Z\u0590-\u05FF\u200f\u200e ]+$')),
    action: yup.string().required().matches(RegExp('^[a-zA-Z\u0590-\u05FF\u200f\u200e ]+$')),
    startDate: yup.date().required(),
    endDate: yup.date().required().when("startDate", (startDate, schema) => startDate && schema.min(startDate)),
}).required();