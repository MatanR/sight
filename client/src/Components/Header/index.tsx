import React from 'react';
import { Column, Row } from '../../Layout'
import { Typography } from '@mui/material'
import EventModal from '../EventModal';

export default ({ headerTitle = '', children }: any) => {
    return <Column>
        <Typography
            sx={{
                padding: '16px',
                fontSize: 24,
                textAlign: 'right',
                fontWeight: 'bold'
            }}
        >
            {headerTitle}
        </Typography>
        <Row sx={{ alignItem: 'center', justifyContent: 'space-evenly' }}>
            <Row width='96%'>
                {children}
            </Row>
        </Row>
    </Column>
}
