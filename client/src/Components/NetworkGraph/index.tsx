import { useCallback } from 'react';
import ReactFlow, {
    addEdge,
    Background,
    useNodesState,
    useEdgesState,
    MiniMap,
    Controls,
} from 'reactflow';
import 'reactflow/dist/style.css';
import { initialEdges, initialNodes } from './nodeEdges';

const NetworkGraph = () => {
    const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
    const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);

    return (
        <ReactFlow
            nodes={nodes}
            edges={edges}
            onNodesChange={onNodesChange}
            onEdgesChange={onEdgesChange}
            fitView
            nodesConnectable={false}
            style={{ direction: 'ltr' }}
        >
            <Controls />
            <Background />
        </ReactFlow>
    );
};

export default NetworkGraph;