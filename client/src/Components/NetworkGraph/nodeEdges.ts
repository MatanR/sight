import { MarkerType } from 'reactflow';

export const colors = {
    DEFAULT: 'rgba(11, 135, 43, 0.2)',
    SHUTDOWN: 'rgba(255, 0, 0, 0.2)',
    UPGRADE: 'rgba(233, 106, 21, 0.2)',
    OTHER: 'rgba(16, 0, 255, 0.2)',
}

export const initialNodes: any = [
    {
        id: 'SYSTEM_A',
        data: { label: 'NESHER (שדרוג)' },
        position: { x: 100, y: 100 },
        className: 'light',
        style: { backgroundColor: colors.UPGRADE, width: 200, height: 200 },
    },
    {
        id: 'SERVER_A.1',
        data: { label: 'SERVER A.1' },
        position: { x: 25, y: 50 },
        parentNode: 'SYSTEM_A',
        extent: 'parent',
    },
    {
        id: 'SERVER_A.2',
        data: { label: 'SERVER A.2' },
        position: { x: 25, y: 100 },
        parentNode: 'SYSTEM_A',
        extent: 'parent',
    },
    {
        id: 'SERVER_A.3',
        data: { label: 'SERVER A.3' },
        position: { x: 25, y: 150 },
        parentNode: 'SYSTEM_A',
        extent: 'parent',
    },
    {
        id: 'SYSTEM_B',
        data: { label: 'MZ' },
        position: { x: 400, y: 100 },
        className: 'light',
        style: { backgroundColor: colors.DEFAULT, width: 200, height: 200 },
    },
    {
        id: 'SERVER_B.1',
        data: { label: 'SERVER B.1' },
        position: { x: 25, y: 50 },
        parentNode: 'SYSTEM_B',
        extent: 'parent',
    },
    {
        id: 'SERVER_B.2',
        data: { label: 'SERVER B.2' },
        position: { x: 25, y: 100 },
        parentNode: 'SYSTEM_B',
        extent: 'parent',
    },
    {
        id: 'SERVER_B.3',
        data: { label: 'SERVER B.3' },
        position: { x: 25, y: 150 },
        parentNode: 'SYSTEM_B',
        extent: 'parent',
    },
    {
        id: 'SYSTEM_C',
        data: { label: 'VOD (השבתה)' },
        position: { x: 100, y: 400 },
        className: 'light',
        style: { backgroundColor: colors.SHUTDOWN, width: 200, height: 200 },
    },
    {
        id: 'SERVER_C.1',
        data: { label: 'SERVER C.1' },
        position: { x: 25, y: 50 },
        parentNode: 'SYSTEM_C',
        extent: 'parent',
    },
    {
        id: 'SERVER_C.2',
        data: { label: 'SERVER C.2' },
        position: { x: 25, y: 100 },
        parentNode: 'SYSTEM_C',
        extent: 'parent',
    },
    {
        id: 'SYSTEM_OCP',
        data: { label: 'OCP' },
        position: { x: 600, y: 350 },
        className: 'light',
        style: { backgroundColor: colors.DEFAULT, width: 200, height: 100 },
    },
    {
        id: 'SERVER_OCP',
        data: { label: 'SERVER OCP' },
        position: { x: 25, y: 50 },
        parentNode: 'SYSTEM_OCP',
        extent: 'parent',
    },
    {
        id: 'SYSTEM_AD',
        data: { label: 'AD' },
        position: { x: 350, y: 350 },
        className: 'light',
        style: { backgroundColor: 'rgba(11, 135, 43, 0.2)', width: 200, height: 100 },
    },
    {
        id: 'SERVER_AD',
        data: { label: 'SERVER AD' },
        position: { x: 25, y: 50 },
        parentNode: 'SYSTEM_AD',
        extent: 'parent',
    },
];

export const initialEdges: any = [
    {
        id: 'A-B',
        source: 'SYSTEM_A',
        target: 'SYSTEM_B',
        animated: true,
        style: { stroke: 'orange' },
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'C-B',
        source: 'SYSTEM_C',
        target: 'SYSTEM_B',
        animated: true,
        style: { stroke: 'red' },
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'AD-A',
        source: 'SYSTEM_AD',
        target: 'SYSTEM_A',
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'AD-B',
        source: 'SYSTEM_AD',
        target: 'SYSTEM_B',
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'AD-C',
        source: 'SYSTEM_AD',
        target: 'SYSTEM_C',
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'OCP-B',
        source: 'SYSTEM_OCP',
        target: 'SYSTEM_B',
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
    {
        id: 'OCP-C',
        source: 'SYSTEM_OCP',
        target: 'SYSTEM_C',
        markerEnd: {
            type: MarkerType.ArrowClosed,
        },
    },
];