import React from 'react'
import { FormControl, Select, InputLabel, MenuItem } from "@mui/material"

const NetworkSelect = ({ }) => {
    return <FormControl sx={{ width: '12%' }}>
        <InputLabel id="demo-simple-select-label">רשת</InputLabel>
        <Select
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            value={"תפוח"}
            label="רשת"
            sx={{ height: '80%' }}
        >
            <MenuItem value={"תפוח"}>תפוח</MenuItem>
        </Select>
    </FormControl>;
}

export default NetworkSelect