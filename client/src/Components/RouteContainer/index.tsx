import React from 'react';
import { Row } from '../../Layout'

interface IRouteContainer {
    HeaderComponent: Function,
    RouteComponent: Function,
    label: string
}

export default ({ HeaderComponent, RouteComponent, label }: IRouteContainer) => {
    return <>
        <Row height="20%">
            <HeaderComponent headerTitle={label} />
        </Row>
        <Row height="80%">
            <RouteComponent />
        </Row>
    </>
}