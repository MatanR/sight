import React from 'react';
import { Column } from '../../Layout'
import Logo from '../../assets/logo.png';
import { List, ListItem, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material'
import { routes } from '../../Routes/routes'
import { useNavigate } from 'react-router-dom'

export default ({ }) => {
    const navigate = useNavigate()

    return <Column sx={{ backgroundColor: '#0B872B1A' }} width={200}>
        <img src={Logo} alt="logo" style={{ scale: '0.5' }} />
        <nav aria-label="main mailbox folders">
            <List>
                {
                    routes.map(({ Icon, label, path }) => {
                        return <ListItem disablePadding key={label}>
                            <ListItemButton onClick={() => navigate(path)}>
                                <ListItemIcon>
                                    <Icon />
                                </ListItemIcon>
                                <ListItemText
                                    primary={<Typography
                                        sx={{
                                            fontSize: 16,
                                            textAlign: 'right',
                                        }}
                                    >
                                        {label}
                                    </Typography>
                                    }
                                />
                            </ListItemButton>
                        </ListItem>
                    })
                }
            </List>
        </nav>
    </Column>
}
