import React from 'react';
import { Box } from '@mui/material'

interface Props {
  children?: React.ReactNode,
  width?: string | number,
  height?: string | number,
  sx?: object
}

export default ({ children, width = '100%', height = '100%', sx = {}, ...props }: Props) => {
  return <Box
    sx={{ height, width, display: 'flex', flexDirection: 'row', ...sx }}
    {...props}
  >
    {children}
  </Box>
}
