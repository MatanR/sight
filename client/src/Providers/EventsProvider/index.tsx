import React from 'react'
import createDataContext from '../createDataContext'
import axios from 'axios'
import { mockData } from './mock'

enum ActionsTypes {
    UPDATE_EVENTS,
}

interface IReducerAction {
    type?: number,
    payload?: {}
}

const reducer: React.Reducer<object, object> = (state: object, action: IReducerAction) => {
    switch (action.type) {
        case ActionsTypes.UPDATE_EVENTS:
            return { ...state, events: action.payload }
        default:
            return state;
    }
}

const fetchEvents = (dispatch: React.Dispatch<IReducerAction>) => {
    return (networkId: string, date: Date) => {
        const controller = new AbortController()

        // const startDate = new Date(date.getFullYear(), date.getMonth(), 1).toISOString()
        // const endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0).toISOString()

        axios.get(`/api/events`, { signal: controller.signal })
            .then(({ data }) => {
                console.log(data)

                const newEvents = data.map((event: any) => {
                    return { ...event, title: event.name }
                })

                dispatch({ type: ActionsTypes.UPDATE_EVENTS, payload: newEvents })
            })

        return () => controller.abort()
    }
}

const actions = { fetchEvents }
const defaultValue = { events: [] }
const { Provider, useDataContext } = createDataContext({ reducer, actions, defaultValue })

export { Provider as EventsProvider, useDataContext as useEvents }