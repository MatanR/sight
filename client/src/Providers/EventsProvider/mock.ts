const randomDate = (start: Date, end: Date) => {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

const randomNumber = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1)) + min;


const titles = ['פעילות ברשת מסווגת', 'השבתה דחופה', 'שדרוג מערכת', 'פעילות שגרתית', 'השבתת מערכת', 'עדכון תשתית']

export const mockData = new Array(20).fill(0).map(_ => {
    const startDate = randomDate(new Date(2022, 10, 1), new Date(2022, 10, 29))
    const endDate = randomDate(startDate, new Date(startDate.getTime() + 1000 * 60 * 60 * 10))

    return {
        startDate,
        endDate,
        title: titles[randomNumber(0, titles.length - 1)],
        action: ['UPGRADE', 'SHUTDOWN', 'OTHER'][randomNumber(0, 2)]
    }
})