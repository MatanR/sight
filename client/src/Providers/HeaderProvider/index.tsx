import React from 'react'
import createDataContext from '../createDataContext'

enum ActionsTypes {
    UPDATE_HEADER_TITLE,
}

interface IReducerAction {
    type?: number,
    payload?: {}
}

const reducer: React.Reducer<object, object> = (state: object, action: IReducerAction) => {
    switch (action.type) {
        case ActionsTypes.UPDATE_HEADER_TITLE:
            return { ...state, headerTitle: action.payload }
        default:
            return state;
    }
}

const changeHeaderTitle = (dispatch: React.Dispatch<IReducerAction>) => {
    return (headerTitle: string) => {
        dispatch({ type: ActionsTypes.UPDATE_HEADER_TITLE, payload: { headerTitle } })
    }
}

const actions = { changeHeaderTitle }
const defaultValue = { headerTitle: '' }
const { Provider, useDataContext } = createDataContext({ reducer, actions, defaultValue })

export { Provider as HeaderProvider, useDataContext as useHeader }