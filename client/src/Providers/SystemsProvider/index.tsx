import React from 'react'
import createDataContext from '../createDataContext'
import axios from 'axios'

enum ActionsTypes {
    UPDATE_SYSTEMS,
}

interface IReducerAction {
    type?: number,
    payload?: {}
}

const reducer: React.Reducer<object, object> = (state: object, action: IReducerAction) => {
    switch (action.type) {
        case ActionsTypes.UPDATE_SYSTEMS:
            return { ...state, systems: action.payload }
        default:
            return state;
    }
}

const fetchSystems = (dispatch: React.Dispatch<IReducerAction>) => {
    return (networkId: string) => {
        const controller = new AbortController()

        axios.get(`/api/systems`,
            { signal: controller.signal })
            .then(({ data }) => {
                dispatch({ type: ActionsTypes.UPDATE_SYSTEMS, payload: data })
            })

        return () => controller.abort()
    }
}

const actions = { fetchSystems }
const defaultValue = { systems: [] }
const { Provider, useDataContext } = createDataContext({ reducer, actions, defaultValue })

export { Provider as SystemsProvider, useDataContext as useSystems }