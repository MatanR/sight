import { ThemeProvider as MuiThemeProvider, createTheme } from '@mui/material/styles';

const theme = createTheme({
    palette: {
        secondary: {
            main: '#7ce3ba'
        },
    },
});

const ThemeProvider = ({ children }) => <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;

export default ThemeProvider;