import React, { createContext, useReducer, useContext } from 'react';

export interface ICreateDataContextProps {
    reducer: React.Reducer<object, object>,
    actions: {
        [key: string]: any
    },
    defaultValue: {
        [key: string]: any
    },
}

const initialContextValue: { [key: string]: any } = {}

export default ({ reducer, actions, defaultValue }: ICreateDataContextProps) => {
    const Context = createContext(initialContextValue)

    const Provider = ({ children }: React.PropsWithChildren) => {
        const [state, dispatch] = useReducer(reducer, defaultValue);

        const boundActions: { [key: string]: any } = {};

        for (const key in actions) {
            boundActions[key] = actions[key](dispatch)
        }

        return (
            <Context.Provider value={{ ...state, ...boundActions }}>
                {children}
            </Context.Provider>
        )
    };

    return { useDataContext: () => useContext(Context), Provider };
}
