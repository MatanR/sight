import React from "react"
import { EventsProvider } from "./EventsProvider"
import { SystemsProvider } from "./SystemsProvider"

export default ({ children }: React.PropsWithChildren) => {
    return <EventsProvider>
        <SystemsProvider>
            {children}
        </SystemsProvider>
    </EventsProvider>
}