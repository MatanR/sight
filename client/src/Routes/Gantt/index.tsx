import React, { useState } from "react"
import Header from "../../Components/Header"
import Calender from "../../Components/Calendar"
import NetworkSelect from "../../Components/NetworkSelect";
import EventModal from "../../Components/EventModal";
import { Button } from '@mui/material'
import InsertInvitationIcon from '@mui/icons-material/InsertInvitation';

export const GanttRoute = ({ ...props }) => {
    return <Calender />
}

export const GanttHeader = ({ ...props }) => {
    const [isOpen, setIsOpen] = useState(false);

    const changeModalState = () => setIsOpen(prev => !prev);

    return <Header headerTitle={props.headerTitle} >
        <NetworkSelect />
        <Button sx={{
            marginRight: '16px',
            display: 'flex',
            justifyContent: 'space-between',
            flexDirection: 'row-reverse',
            height: '80%',
            width: '14%'
        }}
            onClick={changeModalState}
            variant="outlined"
            startIcon={<InsertInvitationIcon />}
        >
            הוספת אירוע
        </Button>
        <EventModal isOpen={isOpen} changeModalState={changeModalState} />
    </Header>
}

