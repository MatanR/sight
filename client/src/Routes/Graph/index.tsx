import React from "react"
import Header from "../../Components/Header"
import NetworkSelect from "../../Components/NetworkSelect"
import { Row } from "../../Layout"
import NetworkGraph from "../../Components/NetworkGraph"

export const GraphRoute = ({ ...props }) => {
    return <Row>
        <NetworkGraph />
    </Row>
}

export const GraphHeader = ({ ...props }) => {
    return <Header headerTitle={props.headerTitle} >
        <NetworkSelect />
    </Header>
}