import React from "react";
import { Routes, Route } from "react-router-dom";
import RouteContainer from "../Components/RouteContainer";
import { routes } from "./routes";

// eslint-disable-next-line import/no-anonymous-default-export
export default () => {
  return (
    <Routes>
      {
      routes.map(({ path, label, RouteComponent, HeaderComopnent }) => {
        return (
          <Route
            key={path}
            path={path}
            element={
              <RouteContainer
                HeaderComponent={HeaderComopnent}
                RouteComponent={RouteComponent}
                label={label}
              />
            }
          ></Route>
        );
      })}
    </Routes>
  );
};
