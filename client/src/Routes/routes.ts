import React from 'react'
import { DateRange, Hub } from '@mui/icons-material'
import { GanttRoute, GanttHeader } from './Gantt'
import { GraphRoute, GraphHeader } from './Graph'

export const routes = [
    {
        path: '/',
        RouteComponent: GanttRoute,
        HeaderComopnent: GanttHeader,
        Icon: DateRange,
        label: 'לוח אירועים'
    },
    {
        path: '/graph',
        RouteComponent: GraphRoute,
        HeaderComopnent: GraphHeader,
        Icon: Hub,
        label: 'גרף רשת'
    }
]