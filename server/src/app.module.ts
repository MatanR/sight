import { Module } from '@nestjs/common';
import { NetworkModule } from './network/network.module';
import { EventModule } from './event/event.module';
import { SystemModule } from './system/system.module';
import { DgraphService } from './dgraph/dgraph.service';
import { ClientModule } from './client/client.module';

@Module({
  imports: [NetworkModule, EventModule, SystemModule, ClientModule],
  controllers: [],
  providers: [DgraphService],
})
export class AppModule {}
