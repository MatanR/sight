import { Injectable } from '@nestjs/common';
import { DgraphClient, DgraphClientStub } from 'dgraph-js-http';

@Injectable()
export class DgraphService {
  public dgraphClient: any;

  constructor() {
    const clientStub = new DgraphClientStub(
      'http://dgraph-access.apps.pnelwhem.eastus.aroapp.io/',
    );
    this.dgraphClient = new DgraphClient(clientStub);
  }
}
