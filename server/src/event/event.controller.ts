import { Controller, Get, Post, Body, Param } from '@nestjs/common';
import { EventService } from './event.service';
import { CreateEventDto } from './dto/create-event.dto';

@Controller('api/events')
export class EventController {
  constructor(private readonly eventService: EventService) { }

  @Post()
  create(@Body() createEventDto: CreateEventDto) {
    return this.eventService.addEvent(createEventDto);
  }

  @Get()
  findAll() {
    return this.eventService.getAllEvents();
  }

  @Get(':eventId')
  getAllNetworkEvents(@Param('eventId') eventId: string) {
    return this.eventService.findOne(+eventId);
  }
}
