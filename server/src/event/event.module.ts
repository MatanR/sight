import { Module } from '@nestjs/common';
import { EventService } from './event.service';
import { EventController } from './event.controller';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Module({
  controllers: [EventController],
  providers: [EventService, DgraphService],
})
export class EventModule {}
