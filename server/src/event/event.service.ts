import { DgraphClient, DgraphClientStub } from 'dgraph-js-http';
import { Body, HttpException, Injectable } from '@nestjs/common';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Injectable()
export class EventService {
  constructor(private dgraphService: DgraphService) { }

  async addEvent(@Body() requestBody) {
    const txn = this.dgraphService.dgraphClient.newTxn();
    const res = await txn.query(`{
      Events(func: type(Event)) {
        uid,
        name,
        manager,
        startDate,
        endDate,
        affects {
          uid,
          name,
          affects {
            uid,
            name
          }
        }
      }
    }`);
    const conflicts = res.data.Events.filter(
      (event) => ((
        (event.affects[0].uid === requestBody.systemId) || (event.affects[0].affects?.findIndex(sys => sys.uid === requestBody.systemId) > -1)
      ) && !((event.startDate > requestBody.endDate) || (event.endDate < requestBody.startDate)))
    );

    if (conflicts.length > 0) {
      throw new HttpException(
        {
          uid: conflicts[0].uid,
          name: conflicts[0].name,
          systemId: conflicts[0].affects[0].uid,
          systemName: conflicts[0].affects[0].name,
          manager: conflicts[0].manager,
        },
        409,
      );
    }
    const newEvent = {
      name: requestBody.name,
      manager: requestBody.manager,
      startDate: requestBody.startDate,
      endDate: requestBody.endDate,
      "dgraph.type": "Event",
      description: requestBody.description,
      action: requestBody.action,
      affects: [
        { uid: requestBody.systemId },
      ],
    };

    const mu = await txn.mutate({ setJson: newEvent, commitNow: true });
    return mu;
  }

  async getAllEvents() {
    const query = `{
        Event(func: type(Event)) {
          uid,
          action,
          affects {
            uid,
            name,
            manager
          },
          description,
          manager,
          name,
          startDate,
          endDate
          }
        }`;

    const clientStub = new DgraphClientStub(
      "http://dgraph-access.apps.pnelwhem.eastus.aroapp.io/"
    );
    const dgraphClient = new DgraphClient(clientStub);

    const res: any = await dgraphClient.newTxn().query(query);

    return res?.data?.Event;
  }

  findOne(networkId: number) {
    return `This action returns a #${networkId} event`;
  }
}
