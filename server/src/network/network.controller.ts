import { Controller, Get, Param } from '@nestjs/common';
import { NetworkService } from './network.service';

@Controller('api/networks')
export class NetworkController {
  constructor(private readonly networkService: NetworkService) { }

  @Get()
  findAll() {
    return this.networkService.findAll();
  }

  @Get(':networkId')
  getNetworkGraphById(@Param('networkId') networkId: string) {
    return this.networkService.getNetworkGraphById(+networkId);
  }
}
