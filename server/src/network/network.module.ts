import { Module } from '@nestjs/common';
import { NetworkService } from './network.service';
import { NetworkController } from './network.controller';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Module({
  controllers: [NetworkController],
  providers: [NetworkService, DgraphService],
})
export class NetworkModule {}
