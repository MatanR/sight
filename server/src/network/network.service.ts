import { Injectable } from '@nestjs/common';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Injectable()
export class NetworkService {
  constructor ( private dgraphService: DgraphService){}
  
  async findAll() {
    // example
    const query = `{
        System(func: type(System)) {
          uid
          name,
          affects {
            name
            manager
          }
        }
      }`;

      console.log(1);

      const res = await this.dgraphService.dgraphClient.newTxn().query(query);
      console.log(res);
      console.log(2);
      
      const ppl = res;
  
      return ppl;
  }

  getNetworkGraphById(networkId: number) {
    return `This action returns a #${networkId} network`;
  }

}
