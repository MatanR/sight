import { Controller, Get, Param } from '@nestjs/common';
import { SystemService } from './system.service';

@Controller('api/systems')
export class SystemController {
  constructor(private readonly systemService: SystemService) { }

  @Get()
  getAll() {
    return this.systemService.getAll();
  }

  // @Get()
  // getSystemsNames() {
  //   return this.systemService.getSystemsNames();
  // }
}
