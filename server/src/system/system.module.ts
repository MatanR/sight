import { Module } from '@nestjs/common';
import { SystemService } from './system.service';
import { SystemController } from './system.controller';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Module({
  controllers: [SystemController],
  providers: [SystemService, DgraphService],
})
export class SystemModule {}
