import { Injectable } from '@nestjs/common';
import { DgraphService } from 'src/dgraph/dgraph.service';

@Injectable()
export class SystemService {
  constructor(private dgraphService: DgraphService) { }

  async getAll() {
    const query = `{
      System(func: type(System)) {
        uid
        name,
        affects {
          uid,
          name,
					description,
          manager,
          status        
        },
        description,
        manager,
        status
        }
      }`;

    const response = await this.dgraphService.dgraphClient.newTxn().query(query);

    return response.data.System;
  }

  async getSystemsNames() {
    const query = `{
      System(func: type(System)) {
          name
        }
      }`;

    const response = await this.dgraphService.dgraphClient.newTxn().query(query);

    const { data: { System } } = response;
    const systemsNames = System.map(x => x.name);

    console.log(systemsNames);

    return systemsNames;
  }
}
